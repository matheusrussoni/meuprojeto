import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.net.MalformedURLException;
import java.net.URL;

public class AppTest {

    private RemoteWebDriver driver;
    private final String[] pages = {
            "http://www.google.com.br",
            "https://www.bing.com",
            "http://www.youtube.com",
            "https://br.yahoo.com",
            "https://www.netshoes.com.br"
    };

    @BeforeMethod
    public void setup() throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setBrowserName("chrome");
        caps.setPlatform(Platform.LINUX);
        driver = new RemoteWebDriver(new URL("http://dsciat.brazilsouth.cloudapp.azure.com:4444/wd/hub"),caps);
    }

    @org.testng.annotations.Test
    public void testGoogle(){
        System.out.println();
        driver.get(pages[0]);
        String returnedTitle = driver.getTitle();
        Assert.assertEquals("Google", returnedTitle);
    }
    @org.testng.annotations.Test
    public void testBing(){
        driver.get(pages[1]);
        String returnedTitle = driver.getTitle();
        Assert.assertEquals("Bing", returnedTitle);
    }
    @org.testng.annotations.Test
    public void testYouTube(){
        driver.get(pages[2]);
        String returnedTitle = driver.getTitle();
        Assert.assertEquals("YouTube", returnedTitle);
    }
    @org.testng.annotations.Test
    public void testYahoo(){
        driver.get(pages[3]);
        String returnedTitle = driver.getTitle();
        Assert.assertEquals("Yahoo", returnedTitle);
    }
    @org.testng.annotations.Test
    public void testG1(){
        driver.get(pages[4]);
        String returnedTitle = driver.getTitle();
        Assert.assertEquals("Loja de Artigos Esportivos Online | Netshoes", returnedTitle);
    }

    @AfterMethod
    public void encerrar(){
        System.out.println("encerrando");
        driver.quit();
    }
}
